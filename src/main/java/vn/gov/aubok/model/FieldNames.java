package vn.gov.aubok.model;

public class FieldNames {
    public static final String PHONE = "phone";
    public static final String NAME = "name";
    public static final String BOOK_CONTAINER_ID = "book_container_id";
    public static final String CODE = "code";
    public static final String OLD_PASSWORD = "oldPassword";
    public static final String NEW_PASSWORD = "newPassword";
    public static final String RESET_CODE = "resetCode";

}
