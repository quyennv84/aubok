package vn.gov.aubok.model;

import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.audiohistories.AudioHistory;
import vn.gov.aubok.domain.book.Book;

import java.io.Serializable;

public class AudioHistoryResponse implements Serializable {

    private String audioName;

    private String audioId;

    private String duration;

    private String bookName;

    private String bookImage;

    private String author;

    private Integer checkPoint;

    public AudioHistoryResponse() {
    }

    public AudioHistoryResponse(Book book, Audio audio, AudioHistory history) {
        this.audioName = audio.getName();
        this.duration = audio.getDuration();
        this.bookName = book.getName();
        this.bookImage = book.getBookCover();
        this.author = book.getAuthor();
        this.checkPoint = history.getCheckPoint();
        this.audioId = audio.getId();
    }

    public String getAudioName() {
        return audioName;
    }

    public void setAudioName(String audioName) {
        this.audioName = audioName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookImage() {
        return bookImage;
    }

    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAudioId() {
        return audioId;
    }

    public void setAudioId(String audioId) {
        this.audioId = audioId;
    }

    public Integer getCheckPoint() {
        return checkPoint;
    }

    public void setCheckPoint(Integer checkPoint) {
        this.checkPoint = checkPoint;
    }
}
