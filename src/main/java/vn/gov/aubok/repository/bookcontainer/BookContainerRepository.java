package vn.gov.aubok.repository.bookcontainer;

import vn.gov.aubok.domain.bookcontainer.BookContainer;
import vn.gov.aubok.repository.core.DomainRepository;

import java.util.List;

public interface BookContainerRepository extends DomainRepository<BookContainer> {
    BookContainer findById(String id);

    List<BookContainer> findAll();

    List<BookContainer> findByIds(List<String> ids);

    List<BookContainer> findAllParents();
}
