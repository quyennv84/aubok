package vn.gov.aubok.repository.bookcontainer;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import vn.gov.aubok.domain.bookcontainer.BookContainer;
import vn.gov.aubok.domain.bookcontainer.BookContainerFieldNames;
import vn.gov.aubok.domain.core.AbstractEntityFieldNames;
import vn.gov.aubok.repository.core.AbstractDomainRepository;

import java.util.List;

@Repository
public class BookContainerRepositoryImpl extends AbstractDomainRepository<BookContainer> implements BookContainerRepository {

    @Override
    public BookContainer findById(String id) {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.DELETED).ne(true).and(AbstractEntityFieldNames.ID).is(id));
        return this.findOne(query, BookContainer.class);
    }

    @Override
    public List<BookContainer> findAll() {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.DELETED).ne(true));
        return this.find(query, BookContainer.class);
    }

    @Override
    public List<BookContainer> findByIds(List<String> ids) {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.DELETED).ne(true).and(AbstractEntityFieldNames.ID).in(ids));
        return this.find(query, BookContainer.class);
    }

    @Override
    public List<BookContainer> findAllParents() {
        Query query = new Query(
                Criteria.where(BookContainerFieldNames.PARENT_CONTAINER).exists(false).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.find(query, BookContainer.class);
    }
}
