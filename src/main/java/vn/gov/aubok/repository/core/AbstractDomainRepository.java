package vn.gov.aubok.repository.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.CollectionUtils;
import vn.gov.aubok.domain.core.AbstractDomainEntity;
import vn.gov.aubok.domain.core.AbstractEntityFieldNames;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class AbstractDomainRepository<E extends AbstractDomainEntity> implements DomainRepository<E> {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AbstractDomainRepository.class);

    @Autowired(required = false)
    protected MongoTemplate mongoTemplate;

    @Override
    public void insertAll(Collection<? extends Object> objectsToSave) {
        mongoTemplate.insertAll(objectsToSave);
    }

    @Override
    public <S extends E> S findById(String id, Class<S> clazz) {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.ID).is(id).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, clazz);
    }

    @Override
    public <S extends E> List<S> findById(List<String> id, Class<S> clazz) {
        Query query = new Query(Criteria.where(AbstractEntityFieldNames.ID).in(id)
                .and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.find(query, clazz);
    }

    @Override
    public <S extends E> S insert(S entity) {
        mongoTemplate.insert(entity);
        return entity;
    }

    @Override
    public <S extends E> S insert(S entity, String collectionName) {
        mongoTemplate.insert(entity, collectionName);
        return entity;
    }

    @Override
    public void insert(Collection<? extends Object> batchToSave, String collectionName) {
        mongoTemplate.insert(batchToSave, collectionName);
    }

    @Override
    public <S extends E> void remove(S entity) {
        mongoTemplate.remove(entity);
    }

    @Override
    public <S extends E> void remove(S entity, String collectionName) {
        mongoTemplate.remove(entity, collectionName);
    }

    @Override
    public <S extends E> void remove(Query query, String collectionName) {
        mongoTemplate.remove(query, collectionName);
    }

    @Override
    public <S extends E> boolean updateMulti(Query query, Update update, Class<S> clazz) {
        return mongoTemplate.updateMulti(query, update, clazz).wasAcknowledged();
    }

    @Override
    public <S extends E> boolean updateMulti(Query query, Update update, Class<S> clazz, String collectionName) {
        return mongoTemplate.updateMulti(query, update, clazz, collectionName).wasAcknowledged();
    }

    @Override
    public <S extends E> boolean updateFirst(Query query, Update update, Class<S> entityClass) {
        return mongoTemplate.updateFirst(query, update, entityClass).wasAcknowledged();
    }

    @Override
    public <S extends E> boolean updateFirst(Query query, Update update, Class<S> entityClass, String collectionName) {
        return mongoTemplate.updateFirst(query, update, entityClass, collectionName).wasAcknowledged();
    }

    @Override
    public <S extends E> List<S> find(Query query, Class<S> clazz) {
        return mongoTemplate.find(query, clazz);
    }

    @Override
    public <S extends E> List<S> find(Query query, Class<S> clazz, String collectionName) {
        return mongoTemplate.find(query, clazz, collectionName);
    }

    @Override
    public <S extends E> S findOne(Query query, Class<S> clazz) {
        return mongoTemplate.findOne(query, clazz);
    }

    @Override
    public <S extends E> S findOne(Query query, Class<S> clazz, String collectionName) {
        return mongoTemplate.findOne(query, clazz, collectionName);
    }

    @Override
    public <S extends E> S update(S entity) {
        mongoTemplate.save(entity);
        return entity;
    }

    @Override
    public <S extends E> S update(S entity, String collectionName) {
        mongoTemplate.save(entity, collectionName);
        return entity;
    }

    @Override
    public <S> long count(Query query, Class<S> entityClass) {
        return mongoTemplate.count(query, entityClass);
    }

    @Override
    public <S> long count(Query query, Class<S> entityClass, String collectionName) {
        return mongoTemplate.count(query, entityClass, collectionName);
    }

    @Override
    public <S extends E> boolean updatePartial(Map<String, Object> queryFieldsByName,
                                               Map<String, Object> updateFieldsByName, Class<S> entityClass) {
        Query query = new Query();
        List<Map.Entry<String, Object>> queryList = new ArrayList<>(queryFieldsByName.entrySet());
        final Criteria criteria = Criteria.where(queryList.get(0).getKey()).is(queryList.get(0).getValue());
        queryList.stream().skip(1).forEach(entry -> criteria.and(entry.getKey()).is(entry.getValue()));
        final Update update = new Update();
        updateFieldsByName.entrySet().stream().forEach(entry -> update.set(entry.getKey(), entry.getValue()));
        return updateFirst(query.addCriteria(criteria), update, entityClass);
    }

    @Override
    public <S extends E> S findById(String id, List<String> fetchedFields, Class<S> clazz) {
        Query query = new Query(Criteria.where(AbstractEntityFieldNames.ID).is(id).and(AbstractEntityFieldNames.DELETED).ne(true));
        for (String f : fetchedFields) {
            query.fields().include(f);
        }
        return this.findOne(query, clazz);
    }

    @Override
    public <S extends E> S findByIdIncludeDeleted(String id, List<String> fetchedFields, Class<S> clazz) {
        Query query = new Query(Criteria.where(AbstractEntityFieldNames.ID).is(id));
        for (String f : fetchedFields) {
            query.fields().include(f);
        }
        return this.findOne(query, clazz);
    }

    public void pickOffNecessaryFields(Collection<String> fetchedFields, Query query) {
        if (CollectionUtils.isEmpty(fetchedFields)) {
            return;
        } else {
            for (String f : fetchedFields) {
                query.fields().include(f);
            }
        }
    }

    @Override
    public void dropCollection(String collectionName) {
        StringBuilder sb = new StringBuilder();
        sb.append("[CR25][LOG] dropCollection(): ");
        sb.append(collectionName);
        logger.info(sb.toString());
        mongoTemplate.dropCollection(collectionName);
    }

}

