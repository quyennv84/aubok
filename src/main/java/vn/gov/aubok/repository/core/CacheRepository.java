package vn.gov.aubok.repository.core;

import com.nimbusds.jose.util.JSONObjectUtils;
import org.joda.time.DateTimeConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import vn.gov.aubok.utils.JsonUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Repository
public class CacheRepository {
    private static final Logger logger = LoggerFactory.getLogger(CacheRepository.class);

    private static final String TENANT_ID = "_T";
    private static final String DATE = "_D";
    private static final String CACHE_ID = "_id";

    @Autowired(required = false)
    RedisTemplate<String, Object>  redisTemplate;

    @Autowired(required = false)
    protected MongoTemplate mongoTemplate;

    public boolean save(String id, Map<Object, Object> values) {
        try {
            String key = createKey(id);
            redisTemplate.opsForHash().putAll(key, values);
            redisTemplate.expire(key, DateTimeConstants.MILLIS_PER_DAY, TimeUnit.MILLISECONDS);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    private String createKey(String id) {
        return id;
    }

    public Map<Object, Object> get(String id, Collection<Object> fields) {
        if (fields == null || fields.isEmpty()) {
            return get(id);//get all
        }
        try {
            String key = createKey(id);
            List<Object> entries = redisTemplate.opsForHash().multiGet(key, fields);
            if (entries == null || entries.isEmpty() || entries.size() != fields.size()) {
                return null;
            }
            Map<Object, Object> result = new HashMap<>();
            Iterator<Object> a = entries.iterator();
            for (Object field : fields) {
                result.put(field, a.next());
            }
            redisTemplate.expire(key, DateTimeConstants.MILLIS_PER_DAY, TimeUnit.MILLISECONDS);
            return result;
        } catch (Exception ex) {
            return null;
        }
    }

    public Map<Object, Object> get(String id) {
        try {
            String key = createKey(id);
            Map<Object, Object> entries = redisTemplate.opsForHash().entries(key);
            redisTemplate.expire(key, DateTimeConstants.MILLIS_PER_DAY, TimeUnit.MILLISECONDS);
            return entries;
        } catch (Exception ex) {
            return null;
        }
    }

    public void deleteCache(List<String> patterns) {
        if (CollectionUtils.isEmpty(patterns)) {
            return;
        }
        Set<String> allKeys = new HashSet<>();
        for (String pattern : patterns) {
            Set<String> keys = findKeys(pattern);
            if (!CollectionUtils.isEmpty(keys)) {
                allKeys.addAll(keys);
            }
        }
        if (!CollectionUtils.isEmpty(allKeys)) {
            deleteFromCache(allKeys);
        }
    }

    public Object getFromCache(String key) {
        try {
            if (redisTemplate.hasKey(key)) {
                return redisTemplate.opsForValue().get(key);
            }
        } catch (Exception ex) {
            logger.error("Exception: " + ex.getMessage());
        }
        return null;
    }

    public boolean containKey(String key) {
        return redisTemplate.hasKey(key);
    }

    public boolean saveToCache(String key, Object value) {
        try {
            if (key != null && !(value instanceof String)) {
                redisTemplate.setValueSerializer(new StringRedisSerializer());
                redisTemplate.opsForValue().set(key, JsonUtils.toJson(value));
            } else {
                redisTemplate.opsForValue().set(key, value);
            }
            redisTemplate.expire(key, DateTimeConstants.MINUTES_PER_HOUR, TimeUnit.MINUTES);
        } catch (Exception ex) {
            logger.error("Exception: " + ex.getMessage());
            return false;
        }
        return true;
    }

    private boolean deleteFromCache(Collection<String> keys) {
        try {
            redisTemplate.delete(keys);
        } catch (Exception ex) {
            logger.error("Exception: " + ex.getMessage());
            return false;
        }
        return true;
    }

    private Set<String> findKeys(String pattern) {
        try {
            return redisTemplate.keys(pattern);
        } catch (Exception ex) {
            logger.error("Exception: " + ex.getMessage());
            return null;
        }
    }

    public void removeTokenByUserIds(Set<String> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            return;
        }
        try {
            Set<String> keys = findKeys("auth*");
            StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
            redisTemplate.setHashKeySerializer(stringRedisSerializer);
            redisTemplate.setHashValueSerializer(stringRedisSerializer);
            Map<Object, Object> hashMap = new HashMap<>();
            hashMap.put("token", "");
            hashMap.put("signedOnUserId", "");
            hashMap.put("signedOnMacAdd", "");
            for (String key : keys) {
                String userId = (String) redisTemplate.boundHashOps(key).get("userId");
                if (userId != null && userIds.contains(userId)) {
                    redisTemplate.opsForHash().putAll(key, hashMap);
                }
            }
        } catch (Exception e) {
            logger.error("Exception: " + e.getMessage());
        } finally {
            JdkSerializationRedisSerializer jdkSerializationRedisSerializer = new JdkSerializationRedisSerializer();
            redisTemplate.setHashKeySerializer(jdkSerializationRedisSerializer);
            redisTemplate.setHashValueSerializer(jdkSerializationRedisSerializer);
        }
    }

    public boolean saveToCache(String key, Object value, long timeout) {
        try {
            if (key != null && !(value instanceof String)) {
                redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
                redisTemplate.opsForValue().set(key, JsonUtils.toJson(value));
            } else {
                redisTemplate.opsForValue().set(key, value);
            }
            redisTemplate.expire(key, timeout, TimeUnit.MILLISECONDS);
        } catch (Exception ex) {
            logger.error("Exception: " + ex.getMessage());
            return false;
        }
        return true;
    }
}

