package vn.gov.aubok.repository.core;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import vn.gov.aubok.domain.core.AbstractDomainEntity;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface DomainRepository<T extends AbstractDomainEntity> {

    <S extends T> S findById(String id, Class<S> clazz);


    <S extends T> List<S> findById(List<String> id, Class<S> clazz);

    <S extends T> S findById(String id, List<String> fetchedFields, Class<S> clazz);

    <S extends T> S findByIdIncludeDeleted(String id, List<String> fetchedFields, Class<S> clazz);

    <S extends T> S findOne(Query query, Class<S> clazz);

    <S extends T> S findOne(Query query, Class<S> clazz, String collectionName);

    <S extends T> List<S> find(Query query, Class<S> clazz);

    <S extends T> List<S> find(Query query, Class<S> clazz, String collectionName);

    default <S extends T> List<S> findAll(Class<S> clazz) {
        return find(new Query(), clazz);
    }

    <S extends T> S insert(S entity);

    <S extends T> S insert(S entity, String collectionName);

    void insert(Collection<? extends Object> batchToSave, String collectionName);

    <S extends T> void remove(S entity);

    <S extends T> void remove(S entity, String collectionName);

    <S extends T> void remove(Query query, String collectionName);

    <S extends T> boolean updateMulti(Query query, Update update, Class<S> clazz);

    <S extends T> boolean updateMulti(Query query, Update update, Class<S> clazz, String collectionName);

    <S extends T> boolean updateFirst(Query query, Update update, Class<S> entityClass);

    <S extends T> boolean updateFirst(Query query, Update update, Class<S> entityClass, String collectionName);

    <S extends T> boolean updatePartial(Map<String, Object> queryFieldsByName, Map<String, Object> updateFieldsByName,
                                        Class<S> entityClass);

    <S extends T> S update(S entity);

    <S extends T> S update(S entity, String collectionName);

    <S> long count(Query query, Class<S> entityClass);

    <S> long count(Query query, Class<S> entityClass, String collectionName);

    void insertAll(Collection<? extends Object> objectsToSave);

    void dropCollection(String collectionName);
}

