package vn.gov.aubok.repository.audiohistories;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import vn.gov.aubok.domain.audiohistories.AudioHistory;
import vn.gov.aubok.domain.audiohistories.AudioHistoryFieldNames;
import vn.gov.aubok.domain.core.AbstractEntityFieldNames;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.domain.user.UserFieldNames;
import vn.gov.aubok.repository.core.AbstractDomainRepository;

import java.util.List;

@Repository
public class AudioHistoriesRepositoryImpl extends AbstractDomainRepository<AudioHistory> implements AudioHistoriesRepository {

    @Override
    public List<AudioHistory> findByUser(String userId) {
        Query query = new Query(
                Criteria.where(AudioHistoryFieldNames.USER_ID).is(userId).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.find(query, AudioHistory.class);
    }

    @Override
    public List<AudioHistory> findLastestItemsByUser(String userId, long skip, int take) {
        Query query = new Query(
                Criteria.where(AudioHistoryFieldNames.USER_ID).is(userId).and(AbstractEntityFieldNames.DELETED).ne(true));
        query.with(new Sort(Sort.Direction.DESC,AbstractEntityFieldNames.UPDATED_DATE));
        query.skip(skip);
        query.limit(take);

        return this.find(query, AudioHistory.class);
    }

    @Override
    public AudioHistory findById(String id) {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.ID).is(id).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, AudioHistory.class);
    }

    @Override
    public AudioHistory findByUserAndAudio(String userId, String audioId) {
        Query query = new Query(
                Criteria.where(AudioHistoryFieldNames.USER_ID).is(userId)
                        .and(AudioHistoryFieldNames.AUDIO_ID).is(audioId)
                        .and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, AudioHistory.class);
    }

    @Override
    public boolean completeAudio(AudioHistory audioHistory) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(AbstractEntityFieldNames.ID).is(audioHistory.getId()).and(AbstractEntityFieldNames.DELETED).ne(true));

        Update update = new Update();
        update.set(AbstractEntityFieldNames.DELETED, true);

        return updateFirst(query, update, AudioHistory.class);
    }

    @Override
    public boolean updateHistory(AudioHistory audioHistory) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(AbstractEntityFieldNames.ID).is(audioHistory.getId()).and(AbstractEntityFieldNames.DELETED).ne(true));

        Update update = new Update();
        update.set(AudioHistoryFieldNames.CHECK_POINT, audioHistory.getCheckPoint());
        update.set(AbstractEntityFieldNames.UPDATED_DATE, System.currentTimeMillis());

        return updateFirst(query, update, AudioHistory.class);
    }
}
