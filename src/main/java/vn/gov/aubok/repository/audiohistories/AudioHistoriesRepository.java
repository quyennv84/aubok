package vn.gov.aubok.repository.audiohistories;

import vn.gov.aubok.domain.audiohistories.AudioHistory;
import vn.gov.aubok.repository.core.DomainRepository;

import java.util.List;

public interface AudioHistoriesRepository extends DomainRepository<AudioHistory> {
    List<AudioHistory> findByUser(String userId);

    List<AudioHistory> findLastestItemsByUser(String userId, long skip, int take);

    AudioHistory findById(String id);

    AudioHistory findByUserAndAudio(String userId, String audioId);

    boolean completeAudio(AudioHistory audioHistory);

    boolean updateHistory(AudioHistory audioHistory);
}
