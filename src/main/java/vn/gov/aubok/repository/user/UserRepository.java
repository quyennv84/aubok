package vn.gov.aubok.repository.user;


import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.repository.core.DomainRepository;

public interface UserRepository extends DomainRepository<AubokUser> {
    AubokUser findByPhone(String phone);

    AubokUser findById(String id);

    AubokUser findByPhoneOrUserName(String input);

    boolean updateUserInfo(AubokUser user);

    boolean updateUserContainers(AubokUser user);

    AubokUser findByResetCode(String resetCode);

    boolean changePassword(AubokUser user);

    boolean removeResetCode(AubokUser user);

    boolean addResetCode(AubokUser user);
}
