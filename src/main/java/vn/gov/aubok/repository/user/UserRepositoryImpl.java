package vn.gov.aubok.repository.user;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import vn.gov.aubok.domain.core.AbstractEntityFieldNames;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.domain.user.UserFieldNames;
import vn.gov.aubok.repository.core.AbstractDomainRepository;

@Repository
public class UserRepositoryImpl extends AbstractDomainRepository<AubokUser> implements UserRepository{
    @Override
    public AubokUser findByPhone(String phone) {
        Query query = new Query(
                Criteria.where(UserFieldNames.PHONE).is(phone).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, AubokUser.class);
    }

    @Override
    public AubokUser findById(String id) {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.ID).is(id).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, AubokUser.class);
    }

    @Override
    public AubokUser findByResetCode(String resetCode) {
        Query query = new Query(
                Criteria.where(UserFieldNames.RESET_CODE).is(resetCode).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, AubokUser.class);
    }

    @Override
    public AubokUser findByPhoneOrUserName(String input) {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.DELETED).ne(true).
                        orOperator(Criteria.where(UserFieldNames.PHONE).is(input),
                                Criteria.where(UserFieldNames.LOGIN_ID).is(input)));
        return this.findOne(query, AubokUser.class);
    }

    @Override
    public boolean updateUserInfo(AubokUser user) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(AbstractEntityFieldNames.ID).is(user.getId()).and(AbstractEntityFieldNames.DELETED).ne(true));

        Update update = new Update();
        update.set(UserFieldNames.NAME, user.getName());
        update.set(UserFieldNames.EMAIL, user.getEmail());
        update.set(AbstractEntityFieldNames.UPDATED_DATE, System.currentTimeMillis());

        return updateFirst(query, update, AubokUser.class);
    }

    @Override
    public boolean changePassword(AubokUser user) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(AbstractEntityFieldNames.ID).is(user.getId()).and(AbstractEntityFieldNames.DELETED).ne(true));

        Update update = new Update();
        update.set(UserFieldNames.PASSWORD, user.getPassword());
        update.set(AbstractEntityFieldNames.UPDATED_DATE, System.currentTimeMillis());

        return updateFirst(query, update, AubokUser.class);
    }


    @Override
    public boolean removeResetCode(AubokUser user) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(AbstractEntityFieldNames.ID).is(user.getId()).and(AbstractEntityFieldNames.DELETED).ne(true));

        Update update = new Update();
        update.set(UserFieldNames.RESET_CODE, null);
        update.set(AbstractEntityFieldNames.UPDATED_DATE, System.currentTimeMillis());

        return updateFirst(query, update, AubokUser.class);
    }

    @Override
    public boolean addResetCode(AubokUser user) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(AbstractEntityFieldNames.ID).is(user.getId()).and(AbstractEntityFieldNames.DELETED).ne(true));

        Update update = new Update();
        update.set(UserFieldNames.RESET_CODE, user.getResetCode());
        update.set(AbstractEntityFieldNames.UPDATED_DATE, System.currentTimeMillis());

        return updateFirst(query, update, AubokUser.class);
    }

    @Override
    public boolean updateUserContainers(AubokUser user) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(AbstractEntityFieldNames.ID).is(user.getId()).and(AbstractEntityFieldNames.DELETED).ne(true));

        Update update = new Update();
        update.set(UserFieldNames.BOOK_CONTAINERS, user.getListBookContainer());
        update.set(AbstractEntityFieldNames.UPDATED_DATE, System.currentTimeMillis());

        return updateFirst(query, update, AubokUser.class);
    }
}
