package vn.gov.aubok.repository.book;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.domain.book.BookFieldNames;
import vn.gov.aubok.domain.core.AbstractEntityFieldNames;
import vn.gov.aubok.repository.core.AbstractDomainRepository;

import java.util.List;
import java.util.regex.Pattern;

@Repository
public class BookRepositoryImpl extends AbstractDomainRepository<Book> implements BookRepository {

    @Override
    public Book findById(String id) {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.ID).is(id).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, Book.class);
    }

    @Override
    public List<Book> findByIds(List<String> ids) {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.ID).in(ids).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.find(query, Book.class);
    }

    @Override
    public List<Book> findByKeyword(String keyword, int skip, int take) {
        Pattern pattern = Pattern.compile(".*"+Pattern.quote(keyword)+".*", Pattern.CASE_INSENSITIVE);

        Query query = new Query(
                Criteria.where(BookFieldNames.NAME).regex(pattern).and(AbstractEntityFieldNames.DELETED).ne(true));
        query.skip(skip);
        query.limit(take);
        return this.find(query, Book.class);
    }

    @Override
    public List<Book> getBooksInContainer(String containerId) {
        Query query = new Query(
                Criteria.where(BookFieldNames.CONTAINER_LIST).is(containerId).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.find(query, Book.class);
    }

    @Override
    public List<Book> getAllBookInContainers(List<String> containerIds) {
        Query query = new Query(
                Criteria.where(BookFieldNames.CONTAINER_LIST).in(containerIds).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.find(query, Book.class);
    }
}
