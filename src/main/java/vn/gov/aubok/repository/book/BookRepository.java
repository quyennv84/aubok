package vn.gov.aubok.repository.book;

import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.repository.core.DomainRepository;

import java.util.List;

public interface BookRepository extends DomainRepository<Book> {
    Book findById(String id);

    List<Book> findByIds(List<String> ids);

    List<Book> findByKeyword(String keyword, int skip, int take);

    List<Book> getBooksInContainer(String containerId);

    List<Book> getAllBookInContainers(List<String> containerIds);
}
