package vn.gov.aubok.repository.code;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import vn.gov.aubok.domain.code.AubokCode;
import vn.gov.aubok.domain.code.AubokCodeFieldNames;
import vn.gov.aubok.domain.core.AbstractEntityFieldNames;
import vn.gov.aubok.repository.core.AbstractDomainRepository;

@Repository
public class AubokCodeRepositoryImpl extends AbstractDomainRepository<AubokCode> implements AubokCodeRepository {

    @Override
    public AubokCode findByCode(String code) {
        Query query = new Query(
                Criteria.where(AubokCodeFieldNames.CODE).is(code).and(AubokCodeFieldNames.ENABLE).ne(true).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, AubokCode.class);
    }

    @Override
    public AubokCode findActiveCodeByContent(String content) {
        Query query = new Query(
                Criteria.where(AubokCodeFieldNames.CODE).is(content).
                        and(AubokCodeFieldNames.ENABLE).ne(false).
                        and(AubokCodeFieldNames.USER_ID).ne(null).
                        and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, AubokCode.class);
    }

    @Override
    public boolean updateActiveStatus(AubokCode aubokCode) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(AbstractEntityFieldNames.ID).is(aubokCode.getId()).and(AbstractEntityFieldNames.DELETED).ne(true));
        Update update = new Update();
        update.set(AubokCodeFieldNames.ENABLE, aubokCode.getEnable());
        update.set(AubokCodeFieldNames.USER_ID, aubokCode.getUserId());
        update.set(AbstractEntityFieldNames.UPDATED_DATE, System.currentTimeMillis());

        return updateFirst(query, update, AubokCode.class);

    }
}
