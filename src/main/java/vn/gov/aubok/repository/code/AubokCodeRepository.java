package vn.gov.aubok.repository.code;

import vn.gov.aubok.domain.code.AubokCode;
import vn.gov.aubok.repository.core.DomainRepository;

import java.util.List;

public interface AubokCodeRepository extends DomainRepository<AubokCode> {
    AubokCode findByCode(String code);

    AubokCode findActiveCodeByContent(String content);

    boolean updateActiveStatus(AubokCode aubokCode);
}
