package vn.gov.aubok.repository.audio;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.audio.AudioFieldNames;
import vn.gov.aubok.domain.core.AbstractEntityFieldNames;
import vn.gov.aubok.repository.core.AbstractDomainRepository;

import java.util.List;

@Repository
public class AudioRepositoryImpl extends AbstractDomainRepository<Audio> implements AudioRepository {

    @Override
    public Audio findById(String id) {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.ID).is(id).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, Audio.class);
    }

    @Override
    public List<Audio> findByIds(List<String> ids) {
        Query query = new Query(
                Criteria.where(AbstractEntityFieldNames.ID).in(ids).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.find(query, Audio.class);
    }

    @Override
    public Audio findByName(String name) {
        Query query = new Query(
                Criteria.where(AudioFieldNames.NAME).in(name).and(AbstractEntityFieldNames.DELETED).ne(true));
        return this.findOne(query, Audio.class);
    }
}
