package vn.gov.aubok.repository.audio;


import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.repository.core.DomainRepository;

import java.util.List;

public interface AudioRepository extends DomainRepository<Audio> {
    List<Audio> findByIds(List<String> id);

    Audio findById(String id);

    Audio findByName(String name);
}
