package vn.gov.aubok.domain.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.CollectionUtils;
import vn.gov.aubok.domain.core.AbstractDomainEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Document(collection = AubokUser.COLLECTION_NAME)
@JsonIgnoreProperties(value = { "roles", "authorities" })
public class AubokUser extends AbstractDomainEntity {

    public static final String COLLECTION_NAME = "users";

    @Field(value = UserFieldNames.LOGIN_ID)
    private String loginId;

    @Field(value = UserFieldNames.NAME)
    private String name;

    @Field(value = UserFieldNames.PASSWORD)
    @JsonIgnore
    private String password;

    @Field(value = UserFieldNames.BOOK_CONTAINERS)
    private List<String> listBookContainer;

    @Field(value = UserFieldNames.PHONE)
    private String phone;

    @Field(value = UserFieldNames.USER_TYPE)
    @JsonIgnore
    private int userType;

    @Field(value = UserFieldNames.EMAIL)
    private String email;

    @Transient
    @JsonIgnore
    private String[] roles;

    @Field(value = UserFieldNames.RESET_CODE)
    private String resetCode;

    public AubokUser() {

    }

    public AubokUser(String loginId, String password, String phone, String email) {
        this.loginId = loginId;
        this.password = password;
        this.phone = phone;
        this.email = email;
        listBookContainer = new ArrayList<>();
        userType = UserFieldNames.NORMAL;
    }

    public AubokUser(String loginId, String password) {
        this.loginId = loginId;
        this.password = password;
        listBookContainer = new ArrayList<>();
        userType = UserFieldNames.NORMAL;
    }

    public AubokUser(String name, String password, String phone) {
        this.name = name;
        this.password = password;
        this.phone = phone;
        this.listBookContainer = new ArrayList<>();
        this.userType = UserFieldNames.NORMAL;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getListBookContainer() {
        return CollectionUtils.isEmpty(listBookContainer) ? new ArrayList<>() : listBookContainer;
    }

    public void setListBookContainer(List<String> listBookContainer) {
        this.listBookContainer = listBookContainer;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        if (roles != null && roles.length > 0) {
            for (String role : roles) {
                authorities.add(new SimpleGrantedAuthority(role));
            }
        }

        return authorities;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResetCode() {
        return resetCode;
    }

    public void setResetCode(String resetCode) {
        this.resetCode = resetCode;
    }
}
