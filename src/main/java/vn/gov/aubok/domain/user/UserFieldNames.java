package vn.gov.aubok.domain.user;

public class UserFieldNames {
    public static final String DEFAULT_PWD = "12345678";

    public static final String LOGIN_ID = "lid";
    public static final String PASSWORD = "psw";
    public static final String PHONE = "pn";
    public static final String BOOK_CONTAINERS = "bcs";
    public static final String EMAIL = "mail";
    public static final String USER_TYPE = "ut";
    public static final String NAME = "nm";
    public static final String RESET_CODE = "rc";

    //permission
    public static final int ADMIN = 1;
    public static final int NORMAL = 2;

}
