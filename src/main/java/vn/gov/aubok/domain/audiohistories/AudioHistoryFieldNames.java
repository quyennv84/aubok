package vn.gov.aubok.domain.audiohistories;

public class AudioHistoryFieldNames {
    public static final String USER_ID = "uid";
    public static final String BOOK_ID = "bid";
    public static final String CHECK_POINT = "cp";
    public static final String AUDIO_ID = "aid";
}
