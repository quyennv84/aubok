package vn.gov.aubok.domain.audiohistories;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import vn.gov.aubok.domain.core.AbstractDomainEntity;

@Document(collection = AudioHistory.COLLECTION_NAME)
public class AudioHistory extends AbstractDomainEntity {

    public static final String COLLECTION_NAME = "audio_histories";

    @Field(value = AudioHistoryFieldNames.USER_ID)
    private String userId;

    @Field(value = AudioHistoryFieldNames.BOOK_ID)
    private String bookId;

    @Field(value = AudioHistoryFieldNames.CHECK_POINT)
    private Integer checkPoint;

    @Field(value = AudioHistoryFieldNames.AUDIO_ID)
    private String audioId;

    public AudioHistory() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getAudioId() {
        return audioId;
    }

    public void setAudioId(String audioId) {
        this.audioId = audioId;
    }

    public Integer getCheckPoint() {
        return checkPoint;
    }

    public void setCheckPoint(Integer checkPoint) {
        this.checkPoint = checkPoint;
    }
}
