package vn.gov.aubok.domain.core;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

public abstract class AbstractDomainEntity implements Serializable {
    @Id
    protected String id;

    @Field(value = AbstractEntityFieldNames.CREATED_DATE)
    protected Long created;

    @Field(value = AbstractEntityFieldNames.UPDATED_DATE)
    protected Long updated;

    @Field(value = AbstractEntityFieldNames.CREATED_BY)
    protected String createdBy;

    @Field(value = AbstractEntityFieldNames.CREATED_BY_NAME)
    protected String createdByName;

    @Field(value = AbstractEntityFieldNames.UPDATED_BY)
    protected String updatedBy;

    @Field(value = AbstractEntityFieldNames.UPDATED_BY_NAME)
    protected String updatedByName;

    @Field(value = AbstractEntityFieldNames.DELETED)
    protected boolean deleted;

    public String getId() {
        return id;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractDomainEntity other = (AbstractDomainEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public boolean notDeleted() {
        return deleted != true;
    }

    public AbstractDomainEntity initNewId() {
        this.id = ObjectId.get().toHexString();
        return this;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public String getUpdatedByName() {
        return updatedByName;
    }

    public void setUpdatedByName(String updatedByName) {
        this.updatedByName = updatedByName;
    }
}
