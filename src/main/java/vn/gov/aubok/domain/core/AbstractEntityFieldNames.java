package vn.gov.aubok.domain.core;

public final class AbstractEntityFieldNames {
    public static final String ID = "_id";
    public static final String CREATED_DATE = "crd";
    public static final String UPDATED_DATE = "upd";
    public static final String CREATED_BY = "crb";
    public static final String CREATED_BY_NAME = "crb_name";
    public static final String UPDATED_BY = "upb";
    public static final String UPDATED_BY_NAME = "upb_name";
    public static final String DELETED = "del";
}

