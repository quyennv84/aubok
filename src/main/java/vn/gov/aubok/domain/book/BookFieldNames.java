package vn.gov.aubok.domain.book;

public class BookFieldNames {
    public static final String NAME = "nm";
    public static final String AUTHOR = "aut";
    public static final String DESCRIPTION = "des";
    public static final String CONTAINER_LIST = "cnrl";
    public static final String AUDIO_LIST = "adl";
    public static final String BOOK_COVER = "bkc";
}
