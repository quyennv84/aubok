package vn.gov.aubok.domain.book;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.bookcontainer.BookContainer;
import vn.gov.aubok.domain.core.AbstractDomainEntity;

import java.util.List;

@Document(collection = Book.COLLECTION_NAME)
public class Book extends AbstractDomainEntity {

    public static final String COLLECTION_NAME = "book";

    @Field(value = BookFieldNames.NAME)
    private String name;

    @Field(value = BookFieldNames.AUTHOR)
    private String author;

    @Field(value = BookFieldNames.BOOK_COVER)
    private String bookCover;

    @Field(value = BookFieldNames.DESCRIPTION)
    private String description;

    @Field(value = BookFieldNames.CONTAINER_LIST)
    private List<String> containerList;

    @Field(value = BookFieldNames.AUDIO_LIST)
    private List<String> audioList;

    @Transient
    private List<Audio> audioItems;

    @Transient
    private List<BookContainer> containerItems;

    public Book() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getContainerList() {
        return containerList;
    }

    public void setContainerList(List<String> containerList) {
        this.containerList = containerList;
    }

    public List<String> getAudioList() {
        return audioList;
    }

    public void setAudioList(List<String> audioList) {
        this.audioList = audioList;
    }

    public String getBookCover() {
        return bookCover;
    }

    public void setBookCover(String bookCover) {
        this.bookCover = bookCover;
    }

    public List<Audio> getAudioItems() {
        return audioItems;
    }

    public void setAudioItems(List<Audio> audioItems) {
        this.audioItems = audioItems;
    }

    public List<BookContainer> getContainerItems() {
        return containerItems;
    }

    public void setContainerItems(List<BookContainer> containerItems) {
        this.containerItems = containerItems;
    }
}
