package vn.gov.aubok.domain.bookcontainer;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import vn.gov.aubok.domain.core.AbstractDomainEntity;

import java.util.List;

@Document(collection = BookContainer.COLLECTION_NAME)
public class BookContainer extends AbstractDomainEntity {

    public static final String COLLECTION_NAME = "container";

    @Field(value = BookContainerFieldNames.NAME)
    private String name;

    @Field(value = BookContainerFieldNames.IMAGE_PATH)
    private String image;

    @Field(value = BookContainerFieldNames.PARENT_CONTAINER)
    private String parentId;

    @Field(value = BookContainerFieldNames.SUB_CONTAINERS)
    private List<String> subContainers;

    @Transient
    private List<BookContainer> subItems;

    public BookContainer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<String> getSubContainers() {
        return subContainers;
    }

    public void setSubContainers(List<String> subContainers) {
        this.subContainers = subContainers;
    }

    public List<BookContainer> getSubItems() {
        return subItems;
    }

    public void setSubItems(List<BookContainer> subItems) {
        this.subItems = subItems;
    }
}
