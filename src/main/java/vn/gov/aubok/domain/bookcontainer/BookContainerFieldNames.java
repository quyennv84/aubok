package vn.gov.aubok.domain.bookcontainer;

public class BookContainerFieldNames {
    public static final String NAME = "nm";
    public static final String IMAGE_PATH = "imp";
    public static final String SUB_CONTAINERS = "sct";
    public static final String PARENT_CONTAINER = "pct";
}
