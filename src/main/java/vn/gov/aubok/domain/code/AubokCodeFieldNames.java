package vn.gov.aubok.domain.code;

public class AubokCodeFieldNames {
    public static final String CODE = "cd";
    public static final String USER_ID = "uid";
    public static final String BOOK_CONTAINER_ID = "bci";
    public static final String ENABLE = "ena";
}
