package vn.gov.aubok.domain.code;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import vn.gov.aubok.domain.core.AbstractDomainEntity;

@Document(collection = AubokCode.COLLECTION_NAME)
public class AubokCode extends AbstractDomainEntity {

    public static final String COLLECTION_NAME = "code";

    @Field(value = AubokCodeFieldNames.USER_ID)
    @JsonIgnore
    private String userId;

    @Field(value = AubokCodeFieldNames.CODE)
    private String code;

    @Field(value = AubokCodeFieldNames.BOOK_CONTAINER_ID)
    @JsonIgnore
    private String bookContainerId;

    @Field(value = AubokCodeFieldNames.ENABLE)
    @JsonIgnore
    private Boolean enable;

    public AubokCode() {
    }

    public AubokCode(String userId, String code, String bookContainerId) {
        this.userId = userId;
        this.code = code;
        this.bookContainerId = bookContainerId;
        this.enable = false;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBookContainerId() {
        return bookContainerId;
    }

    public void setBookContainerId(String bookContainerId) {
        this.bookContainerId = bookContainerId;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public AubokCode(String code, String bookContainerId) {
        this.code = code;
        this.bookContainerId = bookContainerId;
        this.enable = false;
    }
}
