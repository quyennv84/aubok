package vn.gov.aubok.domain.audio;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import vn.gov.aubok.domain.core.AbstractDomainEntity;

@Document(collection = Audio.COLLECTION_NAME)
public class Audio extends AbstractDomainEntity {

    public static final String COLLECTION_NAME = "audios";

    @Field(value = AudioFieldNames.FILE_NAME)
    private String fileName;

    @Field(value = AudioFieldNames.NAME)
    private String name;

    @Field(value = AudioFieldNames.FILE_SIZE)
    private String fileSize;

    @Field(value = AudioFieldNames.DURATION)
    private String duration;

    public Audio() {
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
