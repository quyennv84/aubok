package vn.gov.aubok.domain.audio;

public class AudioFieldNames {
    public static final String FILE_SIZE = "fsz";
    public static final String FILE_NAME = "fnm";
    public static final String DURATION = "drt";
    public static final String NAME = "nm";
}
