package vn.gov.aubok.service.book;

import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.domain.bookcontainer.BookContainer;

import java.util.List;

public interface BookService {
    Book findById(String id);

    List<Book> findByIds(List<String> ids);

    void checkAudioInBook(String bookId, String audioId);

    boolean checkBookWithUser(String bookId);

    List<Book> findByKeyword(String keyword, int skip, int take);

    List<Book> getBooksInContainer(String containerId);
}
