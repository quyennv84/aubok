package vn.gov.aubok.service.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.domain.bookcontainer.BookContainer;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.repository.book.BookRepository;
import vn.gov.aubok.repository.bookcontainer.BookContainerRepository;
import vn.gov.aubok.service.user.UserService;
import vn.gov.aubok.utils.Utils;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private UserService userService;

    @Override
    public Book findById(String id) {
        return bookRepository.findById(id);
    }

    @Override
    public List<Book> findByIds(List<String> ids) {
        return bookRepository.findByIds(ids);
    }

    @Override
    public void checkAudioInBook(String bookId, String audioId) {
        Book book = findById(bookId);
        if(book == null || book.getAudioList().isEmpty()
            || !book.getAudioList().contains(audioId)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public boolean checkBookWithUser(String bookId) {
        Book book = findById(bookId);
        if(book == null || book.getAudioList().isEmpty() || book.getContainerList().isEmpty()){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        AubokUser currentUser = Utils.getCurrentUser();
        if(currentUser == null){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        AubokUser user = userService.findByUsernameOrPhone(currentUser.getLoginId());
        if(user == null || CollectionUtils.isEmpty(user.getListBookContainer())){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        for(String bookContainer : book.getContainerList()){
            if(user.getListBookContainer().contains(bookContainer)){
                return true;
            }
        }

        return false;
    }

    @Override
    public List<Book> findByKeyword(String keyword, int skip, int take) {
        return bookRepository.findByKeyword(keyword, skip, take);
    }

    @Override
    public List<Book> getBooksInContainer(String containerId) {
        return bookRepository.getBooksInContainer(containerId);
    }
}
