package vn.gov.aubok.service.bookcontainer;

import vn.gov.aubok.domain.bookcontainer.BookContainer;

import java.util.List;

public interface BookContainerService {
    BookContainer findById(String id);

    List<BookContainer> findLatestContainers(int take);

    List<BookContainer> findAll();

    List<BookContainer> findByIds(List<String> ids);

    List<BookContainer> findByCurrentUser();

    List<BookContainer> findByUser(String userId);

    List<BookContainer> findAllParents();
}
