package vn.gov.aubok.service.bookcontainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.bookcontainer.BookContainer;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.domain.user.UserFieldNames;
import vn.gov.aubok.repository.bookcontainer.BookContainerRepository;
import vn.gov.aubok.service.user.UserService;
import vn.gov.aubok.utils.Utils;

import java.util.Collections;
import java.util.List;

@Service
public class BookContainerServiceImpl implements BookContainerService {

    @Autowired
    private BookContainerRepository bookContainerRepository;

    @Autowired
    private UserService userService;

    @Override
    public BookContainer findById(String id) {
        return bookContainerRepository.findById(id);
    }

    @Override
    public List<BookContainer> findLatestContainers(int take) {
        return null;
    }

    @Override
    public List<BookContainer> findAll() {
        return bookContainerRepository.findAll();
    }

    @Override
    public List<BookContainer> findByIds(List<String> ids) {
        return bookContainerRepository.findByIds(ids);
    }

    @Override
    public List<BookContainer> findByCurrentUser() {
        AubokUser currentUser = Utils.getCurrentUser();
        if(currentUser == null){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        AubokUser user = userService.findByUsernameOrPhone(currentUser.getLoginId());
        if(user == null){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        if(user.getListBookContainer().isEmpty()){
            return Collections.EMPTY_LIST;
        }

        return bookContainerRepository.findByIds(user.getListBookContainer());
    }

    @Override
    public List<BookContainer> findByUser(String userId) {
        AubokUser user = userService.findById(userId);
        if(user == null || user.getListBookContainer() == null){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        if(user.getListBookContainer().isEmpty()){
            return Collections.EMPTY_LIST;
        }

        return bookContainerRepository.findByIds(user.getListBookContainer());
    }

    @Override
    public List<BookContainer> findAllParents() {
        return bookContainerRepository.findAllParents();
    }


}
