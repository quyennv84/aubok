package vn.gov.aubok.service.audio;

import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.book.Book;

import java.util.List;

public interface AudioService {
    Audio findById(String id);

    List<Audio> findByIds(List<String> ids);
}
