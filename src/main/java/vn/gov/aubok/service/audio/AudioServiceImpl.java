package vn.gov.aubok.service.audio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.repository.audio.AudioRepository;
import vn.gov.aubok.repository.book.BookRepository;

import java.util.List;

@Service
public class AudioServiceImpl implements AudioService {

    @Autowired
    private AudioRepository audioRepository;

    @Override
    public Audio findById(String id) {
        return audioRepository.findById(id);
    }

    @Override
    public List<Audio> findByIds(List<String> ids) {
        if(ids.isEmpty()){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        return audioRepository.findByIds(ids);
    }
}
