package vn.gov.aubok.service.code;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.bookcontainer.BookContainer;
import vn.gov.aubok.domain.code.AubokCode;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.repository.code.AubokCodeRepository;
import vn.gov.aubok.service.bookcontainer.BookContainerService;
import vn.gov.aubok.service.user.UserService;
import vn.gov.aubok.utils.Utils;

import java.util.List;

@Service
public class AubokCodeServiceImpl implements AubokCodeService {

    private final int MAX_CODE_LENGTH = 8;

    @Autowired
    private AubokCodeRepository aubokCodeRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private BookContainerService bookContainerService;

    @Override
    public AubokCode findByContent(String content) {
        return aubokCodeRepository.findByCode(content);
    }

    @Override
    public boolean isExist(String input) {
        return aubokCodeRepository.findByCode(input) != null;
    }

    @Override
    public AubokCode insert(String bookContainerId) {
        BookContainer bookContainer = bookContainerService.findById(bookContainerId);
        if(bookContainer == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        String generatedCode;
        while (true){
            generatedCode = Utils.getAlphaNumericString(MAX_CODE_LENGTH);
            if(!isExist(generatedCode)){
                break;
            }
        }

        AubokCode code = new AubokCode(generatedCode, bookContainerId);
        code.setCreated(System.currentTimeMillis());
        code.setUpdated(System.currentTimeMillis());

        return aubokCodeRepository.insert(code);
    }

    @Override
    public boolean activeAubokCode(String codeContent, String phone, String name) {
        AubokUser user = userService.findByUsernameOrPhone(phone);
        AubokUser newUser = null;

        if(user == null){
            newUser = userService.createNormalUser(phone, name);
        }

        if(aubokCodeRepository.findActiveCodeByContent(codeContent) != null){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        AubokCode aubokCode = aubokCodeRepository.findByCode(codeContent);
        aubokCode.setEnable(true);
        if(user == null){
            aubokCode.setUserId(newUser.getId());
        }else{
            aubokCode.setUserId(user.getId());
        }


        addBookContainerToUser(aubokCode.getUserId(), aubokCode.getBookContainerId());

        return aubokCodeRepository.updateActiveStatus(aubokCode);
    }

    private void addBookContainerToUser(String userId, String bookContainerId){
        AubokUser user = userService.findById(userId);
        if(user == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        List<String> bookContainerList = user.getListBookContainer();
        bookContainerList.add(bookContainerId);

        user.setListBookContainer(bookContainerList);

        userService.updateBookContainers(user);
    }
}
