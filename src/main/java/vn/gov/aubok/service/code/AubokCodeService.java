package vn.gov.aubok.service.code;


import vn.gov.aubok.domain.code.AubokCode;
import vn.gov.aubok.domain.user.AubokUser;

public interface AubokCodeService {
    AubokCode findByContent(String content);

    boolean isExist(String content);

    AubokCode insert(String bookContainerId);

    boolean activeAubokCode(String codeContent, String phone,String name);
}
