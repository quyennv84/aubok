package vn.gov.aubok.service.user;


import vn.gov.aubok.domain.user.AubokUser;

import java.util.List;

public interface UserService {
    List<AubokUser> findAll();

    AubokUser findByUsernameOrPhone(String input);

    AubokUser findById(String id);

    boolean isExist(String input);

    AubokUser createNormalUser(String input, String name);

    AubokUser updateInfo(AubokUser user);

    boolean changePassword(AubokUser user);

    AubokUser updateBookContainers(AubokUser user);

    AubokUser findByResetCode(String resetCode);

    boolean removeResetCode(AubokUser user);
}
