package vn.gov.aubok.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.domain.user.UserFieldNames;
import vn.gov.aubok.repository.user.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<AubokUser> findAll() {
        return userRepository.findAll(AubokUser.class);
    }

    @Override
    public AubokUser findByUsernameOrPhone(String input) {
        return userRepository.findByPhoneOrUserName(input);
    }

    @Override
    public AubokUser findById(String id) {
        return userRepository.findById(id);
    }

    @Override
    public boolean isExist(String input) {
        return userRepository.findByPhoneOrUserName(input) != null;
    }

    @Override
    public AubokUser createNormalUser(String input, String name) {
        AubokUser user = new AubokUser();
        user.setPhone(input);
        user.setName(name);
        user.setPassword(passwordEncoder.encode(UserFieldNames.DEFAULT_PWD));
        user.setListBookContainer(new ArrayList<>());
        user.setUserType(UserFieldNames.NORMAL);
        user.setCreated(System.currentTimeMillis());
        user.setUpdated(System.currentTimeMillis());

        return userRepository.insert(user);
    }

    @Override
    public AubokUser updateInfo(AubokUser user) {
        if(!userRepository.updateUserInfo(user)){
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return user;
    }

    @Override
    public boolean changePassword(AubokUser user) {
        return userRepository.changePassword(user);
    }

    @Override
    public AubokUser updateBookContainers(AubokUser user) {
        if(!userRepository.updateUserContainers(user)){
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return user;
    }

    @Override
    public AubokUser findByResetCode(String resetCode) {
        return userRepository.findByResetCode(resetCode);
    }

    @Override
    public boolean removeResetCode(AubokUser user) {
        return userRepository.removeResetCode(user);
    }
}
