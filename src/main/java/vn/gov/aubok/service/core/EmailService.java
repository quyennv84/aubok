package vn.gov.aubok.service.core;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);
}
