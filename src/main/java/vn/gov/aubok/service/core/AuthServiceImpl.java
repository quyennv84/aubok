package vn.gov.aubok.service.core;

import org.joda.time.DateTimeConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.domain.user.UserFieldNames;
import vn.gov.aubok.repository.audio.AudioRepository;
import vn.gov.aubok.repository.book.BookRepository;
import vn.gov.aubok.repository.core.CacheRepository;
import vn.gov.aubok.repository.user.UserRepository;
import vn.gov.aubok.service.JwtService;
import vn.gov.aubok.service.user.UserService;
import vn.gov.aubok.utils.JsonUtils;
import vn.gov.aubok.utils.Utils;

import java.util.*;

@Service
public class AuthServiceImpl implements AuthService {

    private static final String BLACK_LIST = "black_list";

    @Autowired
    UserService userService;

    @Autowired
    JwtService jwtService;

    @Autowired
    AudioRepository audioRepository;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    EmailService emailService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public boolean isValidCredentials(String input, String password, String macAddress) {
        AubokUser user = userService.findByUsernameOrPhone(input);
        if(user != null && passwordEncoder.matches(password, user.getPassword()) && isLoginOnDevice(user.getId(), macAddress)){
            saveLoginCredential(user.getId(), macAddress);
            return true;
        }
        return false;
    }

    @Override
    public boolean isValidCredentials(String input, String macAddress) {
        AubokUser user = userService.findByUsernameOrPhone(input);
        if(user != null && isLoginOnDevice(user.getId(), macAddress)){
            saveLoginCredential(user.getId(), macAddress);
            return true;
        }
        return false;
    }

    @Override
    public boolean validateAdmin(String input) {
        AubokUser user = userService.findByUsernameOrPhone(input);
        if(user == null || user.getUserType() != UserFieldNames.ADMIN){
            return false;
        }
        return true;
    }

    @Override
    public void saveLoginCredential(String userId, String macAddress) {
        cacheRepository.saveToCache(userId, macAddress, DateTimeConstants.MILLIS_PER_DAY);
    }

    @Override
    public void removeCredential(String token) {
        AubokUser currentUser = Utils.getCurrentUser();
        if(currentUser == null){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        AubokUser user = userService.findByUsernameOrPhone(currentUser.getLoginId());
        if(user == null){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        cacheRepository.deleteCache(Arrays.asList(user.getId()));
        addTokenToBlackList(token);
    }

    private void addTokenToBlackList(String token){
        if(StringUtils.isEmpty(token)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        List<String> blackList;
        if(!cacheRepository.containKey(BLACK_LIST)){
            blackList = new ArrayList<>();
        }else {
            blackList = (List) JsonUtils.fromJson(cacheRepository.getFromCache(BLACK_LIST).toString(), List.class);
        }
        blackList.add(token);
        cacheRepository.saveToCache(BLACK_LIST, blackList, DateTimeConstants.MILLIS_PER_DAY);
    }

    @Override
    public void forgotPassword(String username) {
        AubokUser user = userService.findByUsernameOrPhone(username);
        String resetCode = Utils.getAlphaNumericString(12);
        if(user == null || StringUtils.isEmpty(user.getEmail())){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        user.setResetCode(resetCode);
        if(!userRepository.addResetCode(user)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        String subject = "Reset password";
        String content = "Your reset link \n\r" + generateResetLink(resetCode);
        emailService.sendSimpleMessage(user.getEmail(), subject, content);
    }

    private String generateResetLink(String resetCode){
        return String.format("http://trithucviet.vip/reset-pass?code=%s", resetCode);
    }

    @Override
    public void resetPassword(String resetCode, String password) {
        AubokUser user = userService.findByResetCode(resetCode);
        if(user == null){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        user.setPassword(passwordEncoder.encode(password));

        if(!userService.changePassword(user)){
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
        userService.removeResetCode(user);
    }

    @Override
    public void changePassword(String oldPassword, String password) {
        AubokUser user = userService.findByUsernameOrPhone(Utils.getCurrentUser().getLoginId());
        if(user == null || !passwordEncoder.matches(oldPassword, user.getPassword())){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        user.setPassword(passwordEncoder.encode(password));

        if(!userService.changePassword(user)){
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @Override
    public boolean isTokenInBlackList(String token) {
        if(cacheRepository.containKey(BLACK_LIST)){
            List blackList = (List) JsonUtils.fromJson(cacheRepository.getFromCache(BLACK_LIST).toString(), List.class);
            return blackList.contains(token);
        }

        return false;
    }

    private boolean isLoginOnDevice(String userId, String macAddress){
        if(cacheRepository.containKey(userId)){
            String savedMac = cacheRepository.getFromCache(userId).toString();
            return savedMac.equals(macAddress);
        }
        return true;
    }

    @Override
    public boolean isOwnerAudio(String token, String audioName){
        if(!jwtService.validateTokenLogin(token)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        String userName = jwtService.getUsernameFromToken(token);
        AubokUser user = userService.findByUsernameOrPhone(userName);
        Audio audio = audioRepository.findByName(audioName);

        if(user == null || CollectionUtils.isEmpty(user.getListBookContainer()) || audio == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        List<Book> bookList = bookRepository.getAllBookInContainers(user.getListBookContainer());
        Set<String> audioSet = new HashSet<>();
        for(Book book : bookList){
            audioSet.addAll(book.getAudioList());
        }

        return audioSet.contains(audio.getId());
    }
}
