package vn.gov.aubok.service.core;

public interface AuthService {
    boolean isValidCredentials(String input, String password, String macAddress);

    boolean validateAdmin(String input);

    void saveLoginCredential(String userId, String macAddress);

    void removeCredential(String token);

    void forgotPassword(String username);

    void resetPassword(String resetCode, String password);

    boolean isValidCredentials(String input, String macAddress);

    boolean isOwnerAudio(String token, String audioName);

    void changePassword(String oldPassword, String password);

    boolean isTokenInBlackList(String token);
}
