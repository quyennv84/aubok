package vn.gov.aubok.service.audiohistories;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.audiohistories.AudioHistory;
import vn.gov.aubok.domain.code.AubokCode;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.repository.audio.AudioRepository;
import vn.gov.aubok.repository.audiohistories.AudioHistoriesRepository;
import vn.gov.aubok.service.user.UserService;
import vn.gov.aubok.utils.Utils;

import java.util.Collections;
import java.util.List;

@Service
public class AudioHistoriesServiceImpl implements AudioHistoriesService {

    @Autowired
    private AudioHistoriesRepository audioHistoriesRepository;

    @Autowired
    private UserService userService;

    @Override
    public AudioHistory findById(String id) {
        return audioHistoriesRepository.findById(id);
    }

    @Override
    public List<AudioHistory> findByUser(String userId) {
        return audioHistoriesRepository.findByUser(userId);
    }

    @Override
    public List<AudioHistory> findLastestItemsByUser(String userId, long skip, int take) {
        return audioHistoriesRepository.findLastestItemsByUser(userId, skip, take);
    }

    @Override
    public List<AudioHistory> findLastestItemsByCurrentUser(int take) {
        AubokUser currentUser = Utils.getCurrentUser();
        if(currentUser == null){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        AubokUser user = userService.findByUsernameOrPhone(currentUser.getLoginId());
        if(user == null){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }else if(CollectionUtils.isEmpty(user.getListBookContainer())){
            return Collections.EMPTY_LIST;
        }

        return audioHistoriesRepository.findLastestItemsByUser(user.getId(), 0, take);
    }

    @Override
    public AudioHistory findByUserAndAudio(String userId, String audioId) {
        return audioHistoriesRepository.findByUserAndAudio(userId, audioId);
    }

    @Override
    public AudioHistory completeAudio(AudioHistory history) {
        AudioHistory existHistory = findByUserAndAudio(history.getUserId(), history.getAudioId());
        AubokUser currentUser = Utils.getCurrentUser();
        if(currentUser == null || existHistory == null){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        AubokUser user = userService.findByUsernameOrPhone(currentUser.getLoginId());
        if(user == null || !user.getId().equals(history.getUserId())){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        if(!audioHistoriesRepository.completeAudio(existHistory)){
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return history;
    }

    @Override
    public AudioHistory saveHistory(AudioHistory history) {
        AudioHistory existHistory = findByUserAndAudio(history.getUserId(), history.getAudioId());
        if(existHistory == null){
            history.setCreated(System.currentTimeMillis());
            history.setUpdated(System.currentTimeMillis());

            audioHistoriesRepository.insert(history);
        }else{
            existHistory.setCheckPoint(history.getCheckPoint());
            audioHistoriesRepository.updateHistory(existHistory);
        }

        return history;
    }
}
