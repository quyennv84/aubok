package vn.gov.aubok.service.audiohistories;

import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.audiohistories.AudioHistory;

import java.util.List;

public interface AudioHistoriesService {
    AudioHistory findById(String id);

    List<AudioHistory> findByUser(String userId);

    List<AudioHistory> findLastestItemsByUser(String userId, long skip, int take);

    List<AudioHistory> findLastestItemsByCurrentUser(int take);

    AudioHistory findByUserAndAudio(String userId, String audioId);

    AudioHistory completeAudio(AudioHistory history);

    AudioHistory saveHistory(AudioHistory history);
}
