package vn.gov.aubok.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import vn.gov.aubok.infrastructure.json.JsonSerializationException;
import vn.gov.aubok.infrastructure.json.NullKeySeriliazer;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class JsonUtils {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false).setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);

    static {
        OBJECT_MAPPER.getSerializerProvider().setNullKeySerializer(new NullKeySeriliazer());
        OBJECT_MAPPER.setVisibility(OBJECT_MAPPER.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY).withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
    }

    public static <T> String toJson(T object) {
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (Exception e) {
            throw new JsonSerializationException(e.getMessage(), e);
        }
    }

    public static <T> String toJson(T object, String defaultValue) {
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static <T> T fromJson(String json, Class<T> type) {
        try {
            return OBJECT_MAPPER.<T> readValue(json, type);
        } catch (Exception e) {
            throw new JsonSerializationException(e.getMessage(), e);
        }
    }

    public static <T> T fromJson(String json, String type) {
        try {
            @SuppressWarnings("unchecked")
            Class<T> typeClass = (Class<T>) Class.forName(type);
            return fromJson(json, typeClass);
        } catch (ClassNotFoundException e) {
            throw new JsonSerializationException(e.getMessage(), e);
        }
    }

    public static <T> byte[] toJsonBytes(T value) throws JsonSerializationException {
        try {
            return OBJECT_MAPPER.writeValueAsBytes(value);
        } catch (JsonProcessingException e) {
            throw new JsonSerializationException(e.getMessage(), e);
        }
    }

    public static <T> T fromJsonBytes(byte[] json, Class<T> type) throws JsonSerializationException {
        try {
            return OBJECT_MAPPER.<T> readValue(json, type);
        } catch (Exception e) {
            throw new JsonSerializationException(e.getMessage(), e);
        }
    }

    static final TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
    };

    public static Map<String, Object> toMap(byte[] json) {
        try {
            Map<String, Object> result = OBJECT_MAPPER.readValue(json, typeRef);
            if (result == null) {
                return new HashMap<>(0);
            }
            return result;
        } catch (Exception e) {
            return new HashMap<>(0);
        }
    }

    public static <T> T fromObject(Object value, Class<T> type) throws JsonSerializationException {
        return fromJson(toJson(value), type);
    }

    public static <T extends Collection<?>> T toCollection(String json, Class<T> collectionType, Class<?> elementType)
            throws JsonSerializationException {
        try {
            return OBJECT_MAPPER.readValue(json, constructCollectionType(collectionType, elementType));
        } catch (Exception e) {
            throw new JsonSerializationException("Cannot convert Json to Object type "
                    + constructCollectionType(collectionType, elementType).toString(), e);
        }
    }

    public static JavaType constructCollectionType(Class<? extends Collection<?>> collectionClass,
                                                   Class<?> elementType) {
        return OBJECT_MAPPER.getTypeFactory().constructCollectionType(collectionClass, elementType);
    }
}
