package vn.gov.aubok.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.domain.user.UserFieldNames;
import vn.gov.aubok.model.FieldNames;
import vn.gov.aubok.model.LoginRequest;
import vn.gov.aubok.model.LoginResponse;
import vn.gov.aubok.service.JwtService;
import vn.gov.aubok.service.core.AuthService;
import vn.gov.aubok.service.user.UserService;
import vn.gov.aubok.utils.Utils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/auth")
public class AuthRestController {

    private final static String TOKEN_HEADER = "authorization";

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthService authService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<LoginResponse> login(HttpServletRequest request, @RequestBody LoginRequest loginRequest) {
        if(!authService.isValidCredentials(loginRequest.getUsername(),loginRequest.getMacAddress())){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        String result = jwtService.generateTokenLogin(loginRequest.getUsername());
        HttpStatus httpStatus = HttpStatus.OK;
        return new ResponseEntity<LoginResponse>(new LoginResponse(result), httpStatus);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<AubokUser> register(HttpServletRequest request, @RequestBody AubokUser userRequest) {
        if(StringUtils.isEmpty(userRequest.getPhone()) || StringUtils.isEmpty(userRequest.getName())
               || !Utils.validatePhone(userRequest.getPhone()) ||userService.isExist(userRequest.getPhone())){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        AubokUser user = userService.createNormalUser(userRequest.getPhone(), userRequest.getName());

        return new ResponseEntity<AubokUser>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public ResponseEntity<String> logout(HttpServletRequest request) {
        String authToken = request.getHeader(TOKEN_HEADER);
        authService.removeCredential(authToken);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "/forgot-password", method = RequestMethod.POST)
    public ResponseEntity<String> forgotPassword(HttpServletRequest request, @RequestBody LoginRequest userRequest) {
        authService.forgotPassword(userRequest.getUsername());

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "/reset-password", method = RequestMethod.POST)
    public ResponseEntity<String> resetPassword(HttpServletRequest request, @RequestBody Map<String, Object> data) {
        if(!data.containsKey(FieldNames.RESET_CODE) || !data.containsKey(FieldNames.NEW_PASSWORD)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        String newPassword = (String) data.get(FieldNames.NEW_PASSWORD);
        String resetCode = (String) data.get(FieldNames.RESET_CODE);

        authService.resetPassword(resetCode, newPassword);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "/change-password", method = RequestMethod.POST)
    public ResponseEntity<String> changePassword(HttpServletRequest request, @RequestBody Map<String, Object> data) {
        if(!data.containsKey(FieldNames.OLD_PASSWORD) || !data.containsKey(FieldNames.NEW_PASSWORD)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        String oldPassword = (String) data.get(FieldNames.OLD_PASSWORD);
        String newPassword = (String) data.get(FieldNames.NEW_PASSWORD);

        authService.changePassword(oldPassword, newPassword);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public ResponseEntity<String> logout(HttpServletRequest request, @RequestParam String token) {
        if(!jwtService.validateTokenLogin(token)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<String>(jwtService.getUsernameFromToken(token),HttpStatus.OK);
    }
}