package vn.gov.aubok.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.domain.bookcontainer.BookContainer;
import vn.gov.aubok.service.audio.AudioService;
import vn.gov.aubok.service.book.BookService;
import vn.gov.aubok.service.bookcontainer.BookContainerService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/api")
public class BookRestController {

    @Autowired
    private BookService bookService;

    @Autowired
    private AudioService audioService;

    @Autowired
    private BookContainerService bookContainerService;

    @RequestMapping(value = "/book/{bookId}", method = GET)
    @ResponseBody
    public ResponseEntity<Book> getSpecificBook(HttpServletRequest request, @PathVariable("bookId") String bookId) {
        Book book = bookService.findById(bookId);

        if(book == null || CollectionUtils.isEmpty(book.getContainerList())){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if(!CollectionUtils.isEmpty(book.getAudioList())){
            List<Audio> audioItems = audioService.findByIds(book.getAudioList());
            for(Audio item : audioItems){
                item.setName(null);
            }
            book.setAudioItems(audioItems);
        }

        if(!CollectionUtils.isEmpty(book.getContainerList())){
            book.setContainerItems(bookContainerService.findByIds(book.getContainerList()));
        }

        return new ResponseEntity<Book>(book, HttpStatus.OK);
    }

    @RequestMapping(value = "/container/{containerId}/books", method = GET)
    @ResponseBody
    public ResponseEntity<List<Book>> getBook(HttpServletRequest request, @PathVariable("containerId") String containerId) {
        BookContainer bookContainer = bookContainerService.findById(containerId);
        if(bookContainer == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<Book>>(bookService.getBooksInContainer(bookContainer.getId()), HttpStatus.OK);
    }

    @RequestMapping(value = "/book/search", method = GET)
    @ResponseBody
    public ResponseEntity<List<Book>> searchBooks(HttpServletRequest request,
                                                  @RequestParam String keyword,
                                                  @RequestParam(defaultValue = "0") String skip,
                                                  @RequestParam(defaultValue = "10") String take) {
        int skipParam = Integer.parseInt(skip);
        int takeParam = Integer.parseInt(take);

        return new ResponseEntity<List<Book>>(bookService.findByKeyword(keyword, skipParam, takeParam), HttpStatus.OK);
    }

    @RequestMapping(value = "/container/book", method = GET)
    @ResponseBody
    public ResponseEntity<List<BookContainer>> getListContainer(HttpServletRequest request, @RequestParam String bookId) {
        Book book = bookService.findById(bookId);
        if(book == null || CollectionUtils.isEmpty(book.getContainerList())){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<BookContainer>>(bookContainerService.findByIds(book.getContainerList()), HttpStatus.OK);
    }
}
