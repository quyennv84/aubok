package vn.gov.aubok.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.audiohistories.AudioHistory;
import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.model.AudioHistoryResponse;
import vn.gov.aubok.model.LoginRequest;
import vn.gov.aubok.model.LoginResponse;
import vn.gov.aubok.service.audio.AudioService;
import vn.gov.aubok.service.audiohistories.AudioHistoriesService;
import vn.gov.aubok.service.book.BookService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/user/audio/histories")
public class AudioHistoriesRestController {

    @Autowired
    private AudioHistoriesService audioHistoriesService;

    @Autowired
    private BookService bookService;

    @Autowired
    private AudioService audioService;

    @RequestMapping(value = "/list", method = GET)
    @ResponseBody
    public ResponseEntity<List<AudioHistoryResponse>> listAudioHistories(HttpServletRequest request, @RequestParam String take) {
        int itemCount = Integer.parseInt(take);
        List<AudioHistory> historyList = audioHistoriesService.findLastestItemsByCurrentUser(itemCount);

        List<AudioHistoryResponse> responseList = new ArrayList<>();
        for(AudioHistory history : historyList){
            Book book = bookService.findById(history.getBookId());
            Audio audio = audioService.findById(history.getAudioId());

            responseList.add(new AudioHistoryResponse(book, audio, history));
        }

        return new ResponseEntity<List<AudioHistoryResponse>>(responseList, HttpStatus.OK);
    }

    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public ResponseEntity<AudioHistory> saveHistory(HttpServletRequest request, @RequestBody AudioHistory body) {
        if(body == null){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<AudioHistory>(audioHistoriesService.saveHistory(body), HttpStatus.OK);
    }

    @RequestMapping(value = "/complete", method = RequestMethod.POST)
    public ResponseEntity<AudioHistory> completeAudio(HttpServletRequest request, @RequestBody AudioHistory body) {
        if(body == null){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<AudioHistory>(audioHistoriesService.completeAudio(body), HttpStatus.OK);
    }
}
