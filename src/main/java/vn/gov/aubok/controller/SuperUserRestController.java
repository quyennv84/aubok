package vn.gov.aubok.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.code.AubokCode;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.model.FieldNames;
import vn.gov.aubok.model.LoginRequest;
import vn.gov.aubok.model.LoginResponse;
import vn.gov.aubok.service.JwtService;
import vn.gov.aubok.service.code.AubokCodeService;
import vn.gov.aubok.service.core.AuthService;
import vn.gov.aubok.service.user.UserService;
import vn.gov.aubok.utils.Utils;

@RestController
@RequestMapping("/auth/admin")
public class SuperUserRestController {

    @Autowired
    private JwtService jwtService;

    @Autowired
    private AubokCodeService aubokCodeService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthService authService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<LoginResponse> login(HttpServletRequest request, @RequestBody LoginRequest loginRequest) {
        if(!authService.validateAdmin(loginRequest.getUsername()) || !authService.isValidCredentials(loginRequest.getUsername(), loginRequest.getPassword(), loginRequest.getMacAddress())){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        String result = jwtService.generateTokenLogin(loginRequest.getUsername());
        HttpStatus httpStatus = HttpStatus.OK;
        return new ResponseEntity<LoginResponse>(new LoginResponse(result), httpStatus);
    }

    @RequestMapping(value = "/generate", method = RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> generate(HttpServletRequest request, @RequestBody HashMap<String, Object> body) {
        if(body.isEmpty() || !body.containsKey(FieldNames.BOOK_CONTAINER_ID)){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }

        if(!authService.validateAdmin(Utils.getCurrentUser().getLoginId())){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        String bookContainerId =(String) body.get(FieldNames.BOOK_CONTAINER_ID);

        AubokCode code = aubokCodeService.insert(bookContainerId);

        Map<String, Object> response = new HashMap<>();
        response.put("code", code);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/generate-active", method = RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> generateAndActive(HttpServletRequest request, @RequestBody HashMap<String, Object> body) {
        if(body.isEmpty() || !body.containsKey(FieldNames.PHONE) || !body.containsKey(FieldNames.BOOK_CONTAINER_ID)){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }

        if(!authService.validateAdmin(Utils.getCurrentUser().getLoginId())){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        String phone =(String) body.get(FieldNames.PHONE);
        String bookContainerId =(String) body.get(FieldNames.BOOK_CONTAINER_ID);

        AubokCode code = aubokCodeService.insert(bookContainerId);
        if(!aubokCodeService.activeAubokCode(code.getCode(), phone, null)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        Map<String, Object> response = new HashMap<>();
        response.put("code", code);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
    }
}