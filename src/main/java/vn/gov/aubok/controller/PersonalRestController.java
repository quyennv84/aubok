package vn.gov.aubok.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.domain.bookcontainer.BookContainer;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.service.audio.AudioService;
import vn.gov.aubok.service.book.BookService;
import vn.gov.aubok.service.bookcontainer.BookContainerService;
import vn.gov.aubok.service.user.UserService;
import vn.gov.aubok.utils.Utils;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/user")
public class PersonalRestController {

    @Autowired
    private BookContainerService bookContainerService;

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Autowired
    private AudioService audioService;

    @RequestMapping(value = "/book/{bookId}/audio/{audioId}", method = GET)
    @ResponseBody
    public ResponseEntity<Audio> getPersonalAudios(HttpServletRequest request, @PathVariable("bookId") String bookId, @PathVariable("audioId") String audioId) {
        if(!bookService.checkBookWithUser(bookId)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        bookService.checkAudioInBook(bookId,audioId);

        return new ResponseEntity<Audio>(audioService.findById(audioId), HttpStatus.OK);
    }

    @RequestMapping(value = "/container", method = GET)
    @ResponseBody
    public ResponseEntity<List<BookContainer>> getPersonalContainer(HttpServletRequest request) {
        List<BookContainer> personalContainerList = bookContainerService.findByCurrentUser();
        for(BookContainer parent: personalContainerList){
            if(!CollectionUtils.isEmpty(parent.getSubContainers())){
                List<BookContainer> subItems = bookContainerService.findByIds(parent.getSubContainers());
                parent.setSubItems(subItems);
            }
        }

        return new ResponseEntity<List<BookContainer>>(personalContainerList, HttpStatus.OK);
    }

    @RequestMapping(value = "/container/{containerId}/book/{bookId}", method = GET)
    @ResponseBody
    public ResponseEntity<Book> getDetailBook(HttpServletRequest request, @PathVariable("bookId") String bookId,
                                        @PathVariable("containerId") String containerId) {
        Book book = bookService.findById(bookId);

        if(book == null || CollectionUtils.isEmpty(book.getContainerList())
                || !book.getContainerList().contains(containerId) ||!bookService.checkBookWithUser(bookId)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if(!CollectionUtils.isEmpty(book.getAudioList())){
            book.setAudioItems(audioService.findByIds(book.getAudioList()));
        }

        if(!CollectionUtils.isEmpty(book.getContainerList())){
            book.setContainerItems(bookContainerService.findByIds(book.getContainerList()));
        }

        return new ResponseEntity<Book>(book, HttpStatus.OK);
    }

    @RequestMapping(value = "/info", method = GET)
    @ResponseBody
    public ResponseEntity<AubokUser> updateUser(HttpServletRequest request) {
        AubokUser currentUser = Utils.getCurrentUser();
        if(currentUser == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<AubokUser>(userService.findByUsernameOrPhone(currentUser.getLoginId()), HttpStatus.OK);
    }
}
