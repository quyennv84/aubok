package vn.gov.aubok.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.domain.bookcontainer.BookContainer;
import vn.gov.aubok.service.audio.AudioService;
import vn.gov.aubok.service.book.BookService;
import vn.gov.aubok.service.bookcontainer.BookContainerService;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/api/container")
public class BookContainerRestController {

    @Autowired
    private BookContainerService bookContainerService;

    @RequestMapping(value = "/list", method = GET)
    @ResponseBody
    public ResponseEntity<List<BookContainer>> allBookContainer(HttpServletRequest request, @RequestParam(required = false) String containerId) {
        List<BookContainer> parentList;
        if(StringUtils.isEmpty(containerId)){
            parentList = bookContainerService.findAllParents();
        }else {
            BookContainer parentContainer = bookContainerService.findById(containerId);
            if(parentContainer == null || CollectionUtils.isEmpty(parentContainer.getSubContainers())){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }else {
                parentList = bookContainerService.findByIds(parentContainer.getSubContainers());
            }
        }

        for(BookContainer parent: parentList){
            if(!CollectionUtils.isEmpty(parent.getSubContainers())){
                List<BookContainer> subItems = bookContainerService.findByIds(parent.getSubContainers());
                parent.setSubItems(subItems);
            }
        }

        return new ResponseEntity<List<BookContainer>>(parentList, HttpStatus.OK);
    }
}
