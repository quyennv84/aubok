package vn.gov.aubok.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.audio.Audio;
import vn.gov.aubok.domain.book.Book;
import vn.gov.aubok.service.audio.AudioService;
import vn.gov.aubok.service.book.BookService;
import vn.gov.aubok.service.core.AuthService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/api")
public class AudioRestController {

    @Autowired
    private AudioService audioService;

    @Autowired
    private AuthService authService;

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/book/{bookId}/list", method = GET)
    @ResponseBody
    public ResponseEntity<List<Audio>> allAudiosInBook(HttpServletRequest request, @PathVariable("bookId") String bookId) {
        Book book = bookService.findById(bookId);

        if(book == null || CollectionUtils.isEmpty(book.getAudioList())){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<List<Audio>>(audioService.findByIds(book.getAudioList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/audio/check", method = GET)
    @ResponseBody
    public ResponseEntity<String> checkOwnership(HttpServletRequest request, @RequestParam String token, @RequestParam String audioName) {
        if(!authService.isOwnerAudio(token, audioName)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
