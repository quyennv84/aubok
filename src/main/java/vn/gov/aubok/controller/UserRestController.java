package vn.gov.aubok.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.user.AubokUser;
import vn.gov.aubok.model.LoginRequest;
import vn.gov.aubok.model.LoginResponse;
import vn.gov.aubok.service.JwtService;
import vn.gov.aubok.service.core.AuthService;
import vn.gov.aubok.service.user.UserService;
import vn.gov.aubok.utils.Utils;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


@RestController
@RequestMapping("/user")
public class UserRestController {

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthService authService;

    @GetMapping(value = "/list")
    public ResponseEntity<List<AubokUser>> listUser(HttpServletRequest request) {
        return new ResponseEntity<List<AubokUser>>(userService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/search/{input}", method = GET)
    @ResponseBody
    public ResponseEntity<AubokUser> register(HttpServletRequest request, @PathVariable("input") String input) {
        if(StringUtils.isEmpty(input)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        if(!userService.isExist(input)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<AubokUser>(userService.findByUsernameOrPhone(input), HttpStatus.OK);
    }

    @RequestMapping(value = "/update", method = POST)
    @ResponseBody
    public ResponseEntity<AubokUser> updateUser(HttpServletRequest request, @RequestBody AubokUser updateUser) {
        if(ObjectUtils.isEmpty(updateUser) || StringUtils.isEmpty(updateUser.getId())){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<AubokUser>(userService.updateInfo(updateUser), HttpStatus.OK);
    }
}
