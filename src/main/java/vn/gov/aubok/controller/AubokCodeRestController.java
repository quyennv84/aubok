package vn.gov.aubok.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import vn.gov.aubok.domain.code.AubokCode;
import vn.gov.aubok.model.FieldNames;
import vn.gov.aubok.service.JwtService;
import vn.gov.aubok.service.code.AubokCodeService;
import vn.gov.aubok.service.core.AuthService;
import vn.gov.aubok.service.user.UserService;
import vn.gov.aubok.utils.Utils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/code")
public class AubokCodeRestController {

    @Autowired
    private AubokCodeService aubokCodeService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthService authService;

    @RequestMapping(value = "/active", method = RequestMethod.POST)
    public ResponseEntity<String> activeCode(HttpServletRequest request, @RequestBody HashMap<String, Object> body) {
        if(body.isEmpty() || !body.containsKey(FieldNames.CODE) || !body.containsKey(FieldNames.PHONE)){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }

        String content =(String) body.get(FieldNames.CODE);
        String phone = (String) body.get(FieldNames.PHONE);
        String name = (String) body.get(FieldNames.NAME);

        if(!Utils.validatePhone(phone)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        if(!aubokCodeService.activeAubokCode(content, phone, name)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<String>(content, HttpStatus.OK);
    }
}
